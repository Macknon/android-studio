package com.e.macknon;

import com.e.macknon.ServerStatus;
import retrofit2.Call;

public interface iPingWService {
    Call <ServerStatus> getStatus();
}

